import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {VariantEnum} from "./interfaces/variant-enum";
import {OptionValue, sources} from "./interfaces/sources";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {RegFormViewerComponent} from "./reg-form-viewer/reg-form-viewer.component";
import {RegData} from "./interfaces/reg-data";
import {DialogCloseType} from "./interfaces/dialog-close-type";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public customForm: FormGroup;
  public variants: typeof VariantEnum = VariantEnum;
  public sources: OptionValue[] = sources;

  private dialogRef: MatDialogRef<RegFormViewerComponent>;

  constructor(private matDialog: MatDialog, private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.customForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      variant: new FormControl(VariantEnum.BASIC),
      terms: new FormControl(false, Validators.requiredTrue),
      months: new FormControl(1),
      source: new FormControl('')
    });
  }

  public resetForm(): void {
    this.customForm.reset({
      firstName: '',
      lastName: '',
      email: '',
      variant: VariantEnum.BASIC,
      terms: false,
      months: 1,
      source: ''
    });
  }

  public submit(): void {
    this.dialogRef = this.matDialog.open(RegFormViewerComponent, {
      data: this.customForm.getRawValue() as RegData,
      width: '480px'
    });
    this.dialogRef.componentInstance.dialogClose$.subscribe(this.dialogCloseHandler.bind(this));
  }

  private dialogCloseHandler(closeType: DialogCloseType): void {
    if (closeType === DialogCloseType.ACCEPT) {
      this.snackBar.open('Dziękujemy za rejestrację w naszym portalu', 'Zamknij', {
        duration: 3000,
      });
    }
    this.dialogRef.close();
  }
}
