import {VariantEnum} from "./variant-enum";
import {SourceEnum} from "./sources";

export interface RegData {
  firstName: string;
  lastName: string;
  email: string;
  variant: VariantEnum;
  terms: boolean;
  months: number
  source: SourceEnum;
}
