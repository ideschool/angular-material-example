export interface OptionValue {
  label: string;
  value: SourceEnum;
}

export enum SourceEnum {
  friends,
  internet,
  socialMedia,
}

export const sources: OptionValue[] = [
  {
    label: 'Znajomi',
    value: SourceEnum.friends,
  },
  {
    label: 'Internet',
    value: SourceEnum.internet,
  },
  {
    label: 'Media społecznościowe',
    value: SourceEnum.socialMedia,
  }
];
