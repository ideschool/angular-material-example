import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegFormViewerComponent } from './reg-form-viewer.component';

describe('RegFormViewerComponent', () => {
  let component: RegFormViewerComponent;
  let fixture: ComponentFixture<RegFormViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegFormViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegFormViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
