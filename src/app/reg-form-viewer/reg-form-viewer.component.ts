import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {RegData} from "../interfaces/reg-data";
import {Observable, Subject} from "rxjs";
import {DialogCloseType} from "../interfaces/dialog-close-type";

@Component({
  selector: 'app-reg-form-viewer',
  templateUrl: './reg-form-viewer.component.html',
  styleUrls: ['./reg-form-viewer.component.scss']
})
export class RegFormViewerComponent implements OnInit {
  public dialogClose$: Observable<DialogCloseType>;
  private dialogCloseSubject: Subject<DialogCloseType> = new Subject<DialogCloseType>();

  constructor(@Inject(MAT_DIALOG_DATA) public regData: RegData) {
    this.dialogClose$ = this.dialogCloseSubject.asObservable();
  }

  ngOnInit(): void {
  }

  public accept(): void {
    this.dialogCloseSubject.next(DialogCloseType.ACCEPT);
  }
}
